cmake_minimum_required(VERSION 2.8.11)
project(nox)

option(NOX_INSTALL "Install libraries and headers to the system" ON)
option(NOX_BUILD_SHARED "Build a shared library version (currently not working with MSVC!)" OFF)
option(NOX_BUILD_STATIC "Build a static library version" ON)
option(NOX_BUILD_TEST "Create the test programs" ON)

option(NOX_RESOURCEPROVIDER_BOOST "Use Boost.Filesystem as a provider for resources." ON)
option(NOX_RESOURCE_OGG "Support loading ogg/vorbis files through the resource cache." ON)
option(NOX_AUDIO_OPENAL "Support playing audio through OpenAL." ON)
option(NOX_APPLICATION_SDL2 "Use SDL2 as a backend for the Application." ON)
option(NOX_WINDOW_SDL2 "Support using a window with SDL2. Requires NOX_APPLICATION_SDL2" ON)
option(NOX_GRAPHICS_OPENGL "Support displaying graphics using OpenGL." ON)
option(NOX_STORAGE_BOOST "Use Boost.Filesystem to access the data storage" ON)
option(NOX_PHYSICS_BOX2D "Use Box2D for the physics backend." ON)
set(NOX_RESOURCE_JSON ON)

set(NOX_LIB_NAME nox)
set(NOX_VERSION_MAJOR 0)
set(NOX_VERSION_MINOR 1)
set(NOX_VERSION_PATCH 0)
set(NOX_VERSION "${NOX_VERSION_MAJOR}.${NOX_VERSION_MINOR}.${NOX_VERSION_PATCH}")
set(NOX_API_VERSION "${NOX_VERSION_MAJOR}.${NOX_VERSION_MINOR}")

set(NOX_SRC_DIR "${CMAKE_CURRENT_SOURCE_DIR}/src")
set(NOX_INCLUDE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/include")
set(NOX_TEST_DIR "${CMAKE_CURRENT_SOURCE_DIR}/test")
set(NOX_THIRDPARTY_DIR "${CMAKE_CURRENT_SOURCE_DIR}/third_party")
set(NOX_GLM_INCLUDE_DIR "${NOX_THIRDPARTY_DIR}/glm/")

add_subdirectory(${NOX_THIRDPARTY_DIR})

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake/Modules/")

include(cotire)
set(COTIRE_UNITY_SOURCE_MAXIMUM_NUMBER_OF_INCLUDES "-j")

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
	if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" AND CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.8)
		message(WARNING "GCC versions below 4.8 might not be able to build because of missing C++11 support.")
	endif()

	include(CheckCXXCompilerFlag)

	set(HAS_CXX14 OFF)

	set(CXX14_FLAG -std=c++14)
	check_cxx_compiler_flag(${CXX14_FLAG} HAS_CXX14_FLAG)

	if (HAS_CXX14_FLAG)
		set(HAS_CXX14 ON)
	elseif (NOT HAS_CXX14)
		set(CXX14_FLAG -std=c++1y)
		check_cxx_compiler_flag(${CXX14_FLAG} HAS_CXX1Y_FLAG)
	endif ()

	if (HAS_CXX1Y_FLAG)
		set(HAS_CXX14 ON)
	elseif (NOT HAS_CXX14)
		set(CXX14_FLAG "-std=c++1y -stdlib=libc++")
		check_cxx_compiler_flag(${CXX14_FLAG} HAS_CXX1Y_STDLIB_FLAG)
	endif ()

	if (HAS_CXX1Y_STDLIB_FLAG)
		set(HAS_CXX14 ON)
	endif ()

	if (HAS_CXX14)
		set(NOX_CXX_FLAGS "${NOX_CXX_FLAGS} ${CXX14_FLAG}")
		message(STATUS "Setting c++ standard flag to ${CXX14_FLAG}")
	else ()
		message(FATAL_ERROR "Compiler does not support C++14 (${CXX14_FLAG})")
	endif ()

	check_cxx_compiler_flag(-fdiagnostics-color HAS_COLOR_OUTPUT)

	if (HAS_COLOR_OUTPUT)
		set(NOX_CXX_FLAGS "${NOX_CXX_FLAGS} -fdiagnostics-color=auto")
	endif ()

	check_cxx_compiler_flag(-flto HAS_LTO)
	check_cxx_compiler_flag(-Ofast HAS_OPTIMIZE_FAST)
	check_cxx_compiler_flag(-ffinite-math-only HAS_FINITE_MATH)
	check_cxx_compiler_flag(-s HAS_STRIP_SYMBOLS)

	if (HAS_LTO)
		message(STATUS "Enabling link-time optimizations (-flto)")
		set(NOX_CXX_FLAGS_RELEASE "${NOX_CXX_FLAGS_RELEASE} -flto")
		set(NOX_LINKER_FLAGS_RELEASE "${NOX_LINKER_FLAGS_RELEASE} -flto")

		if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
			# Need to use gcc-ar and gcc-ranlib for LTO with static libraries.
			# See http://gcc.gnu.org/gcc-4.9/changes.html
			set(CMAKE_AR gcc-ar)
			set(CMAKE_RANLIB gcc-ranlib)
		endif ()
	endif ()

	if (HAS_OPTIMIZE_FAST)
		message(STATUS "Enabling -Ofast optimization.")
		set(NOX_CXX_FLAGS_RELEASE "${NOX_CXX_FLAGS_RELEASE} -Ofast")
	endif ()

	if (HAS_FINITE_MATH)
		message(STATUS "Enabling -ffinite-math-only optimization.")
		set(NOX_CXX_FLAGS_RELEASE "${NOX_CXX_FLAGS_RELEASE} -ffinite-math-only")
	endif ()

	if (HAS_STRIP_SYMBOLS)
		message(STATUS "Stripping symbols (-s)")
		set(NOX_LINKER_FLAGS_RELEASE "${NOX_LINKER_FLAGS_RELEASE} -s")
	endif ()
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
	if (NOT CMAKE_LIBRARY_ARCHITECTURE)
		if (CMAKE_SIZEOF_VOID_P EQUAL 8)
			set(CMAKE_LIBRARY_ARCHITECTURE "x64")
		else ()
			set(CMAKE_LIBRARY_ARCHITECTURE "x86")
		endif ()
	endif ()

	set(NOX_CXX_FLAGS "${NOX_CXX_FLAGS} /MP")
else ()
	message(WARNING "You are using an unsupported compiler. Compilation has only been tested with GCC.")
endif ()

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
	set(NOX_WARNINGS "${NOX_WARNINGS} -Wall -Wextra -pedantic -Wconversion -Wsign-conversion -Wuninitialized -Wdelete-non-virtual-dtor")
endif ()

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
	set(NOX_WARNINGS "${NOX_WARNINGS} -Weverything -Wno-c++98-compat -Wno-c++98-compat-pedantic -Wno-padded -Wno-shadow -Wno-float-equal")
endif ()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${NOX_CXX_FLAGS}")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${NOX_CXX_FLAGS_RELEASE}")
set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} ${NOX_LINKER_FLAGS_RELEASE}")

add_subdirectory(${NOX_SRC_DIR})

if (NOX_BUILD_SHARED)
	set(NOX_LINK_TARGET nox-shared)
else ()
	set(NOX_LINK_TARGET nox-static)
endif ()

if (NOX_BUILD_TEST)
	add_subdirectory(${NOX_TEST_DIR})
endif ()
