/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_DATASTORAGEBOOST_H_
#define NOX_APP_RESOURCE_DATASTORAGEBOOST_H_

#include "IDataStorage.h"

#include <boost/filesystem.hpp>
#include <exception>

namespace nox { namespace app
{
namespace storage
{

class DataStorageBoost: public IDataStorage
{
public:
	class NonExistentPathException: public IntializationException
	{
	public:
		virtual const char* what() const NOX_NOEXCEPT override;
	};

	class NotADirectoryException: public IntializationException
	{
	public:
		virtual const char* what() const NOX_NOEXCEPT override;
	};

	DataStorageBoost();

	void init(const std::string& storageDirectory) override;
	bool fileExists(const std::string& filePath) const override;
	void openReadableFile(const std::string& filePath, std::ifstream& inputStream) override;
	void openWritableFile(const std::string& filePath, std::ofstream& outputStream, bool append = false) override;
	std::string readFileContent(const std::string& filePath) override;
	void writeFileContent(const std::string& filePath, const std::string& content, bool append = false) override;
	void removeFolder(const std::string& directoryPath) override;

private:
	void ensureDirectoryExistence(const boost::filesystem::path& path) const;

	boost::filesystem::path dataStoragePath;
};

}
} }

#endif
