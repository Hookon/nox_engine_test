/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_STENCILEDTILEDTEXTURERENDERER_H_
#define NOX_APP_GRAPHICS_STENCILEDTILEDTEXTURERENDERER_H_

#include "TiledTextureRenderer.h"
#include "../GeometrySet.h"

namespace nox { namespace app
{
namespace graphics
{

class StenciledTiledTextureRenderer : public TiledTextureRenderer
{
public:
	StenciledTiledTextureRenderer();
	
	void setup(RenderData& renderData) override;
	void render(RenderData& renderData, const glm::mat4& viewProjectionMatrix, const GLint stencilRef, const GLenum stencilTest);
	void stencilIncrement(RenderData& renderData, const glm::mat4& viewProjectionMatrix);
	void stencil(RenderData& renderData, const glm::mat4& viewProjectionMatrix, const GLint ref);
	void stencilAndRender(RenderData& renderData, const glm::mat4& viewProjectionMatrix);
	
	void addStencilGeometry(const std::shared_ptr<GeometrySet>& set);
	void removeStencilGeometry(const std::shared_ptr<GeometrySet>& set);
	
	void notifyGeometryChange();
	
private:
	void setupTerrainStencil(RenderData& renderData);
	void renderStencil(RenderData& renderData, const glm::mat4& viewProjectionMatrix);
	
	GLuint stencilVAO;
	GLuint stencilVBO;

	bool stencilChanged;
	bool hasStencilObjects;
	
	size_t numTriangleCoords;
	
	std::vector<std::shared_ptr<GeometrySet>> geometrySets;
};

}
} }

#endif
