/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_MATH_GEOMETRY_H_
#define NOX_MATH_GEOMETRY_H_

#include <glm/vec2.hpp>
#include "Circle.h"
#include "Line.h"
#include "Box.h"

namespace nox { namespace math
{

/**
 * Intersection between a line and a circle.
 *
 * firstIntersection and secondIntersection are only valid
 * if the line intersected the circle. This is indicated by
 * the intersected boolean.
 */
struct CircleLineIntersection
{
	CircleLineIntersection():
		intersected(false)
	{}

	//! Indicates if the line intersected the circle.
	bool intersected;

	//! First intersection between line and circle.
	glm::vec2 firstIntersection;

	//! Second intersection between line and circle.
	glm::vec2 secondIntersection;
};

/**
 * Find the circumference of a circle.
 */
float findCircleCircumference(const float radius);

/**
 * Find the intersections, if any, between a line and a circle.
 * @param circle Circle to check for intersection with.
 * @param line Line to check for intersection with.
 * @return Intersection between line and circle.
 */
CircleLineIntersection findCircleLineIntersections(const Circle& circle, const Line& line);

/**
 * Checks if it is possible for the circle to overlap the AABB box.
 * Simply uses the perimiter of the AABB box and the radius of the circle compared to the distance between the centers.
 * @returns True if its possible for them to overlap.
 */
bool simpleCircleAabbRangeCheck(const Circle& circle, const Box<glm::vec2>& aabb);

} }

#endif
