/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_MATH_INTERPOLATION_H_
#define NOX_MATH_INTERPOLATION_H_

#include <glm/vec2.hpp>
#include <glm/trigonometric.hpp>

namespace nox { namespace math
{

/**
 * Linear interpolation from a to b using where t is between [0 - 1]
 * @param a when t is 0 this is where we are.
 * @param b when t is 1 this is where we are.
 * @param t what we base the interpolation on. [0 - 1]
 */
float interpolateLinear(float a, float b, float t);

/**
 * Quadratic interpolation from a through b and to c.
 * Done by linear interpolation of the linear interpolation results of a to b, and b to c.
 * @param a where start when t is 0.
 * @param b the first end point.
 * @param c where we are ending.
 * @param t [0 - 1] basis of the interpolation.
 */
float interpolateQuadratic(float a,float b,float c,float t);

/**
 * Cubic interpolation from a through b, c and to d.
 * Done by using quadratic interpolation results from a - b - c, and b - c - d.
 * @param a our start.
 * @param b second point to base the function on.
 * @param c third point to base the function on.
 * @param d the final point of the interpolation.
 * @param t [0 - 1] basis of the interpolation.
 */
float interpolateQubic(float a,float b,float c,float d,float t);

/**
 * Cosine interpolation from a to b using where t is between [0 - 1]
 * @param a when t is 0 this is where we are.
 * @param b when t is 1 this is where we are.
 * @param t what we base the interpolation on. [0 - 1]
 */
float interpolateCosine(float a, float b, float t);

/**
 * Linear interpolation on an angle.
 * Will find the difference and interpolate the shortest angle from a to b.
 * @param angleA the angle where we start when t is 0.
 * @param angleB the angle we are going towards, when t is 1 we are here.
 * @param t we base the interpolation on this.
 */
float interpolateAngleLinear(float angleA, float angleB, float t);


inline float interpolateLinear(float a, float b, float t)
{
	return ((b - a) * t) + a;
}

inline float interpolateQuadratic(float a, float b, float c, float t)
{
	return interpolateLinear(interpolateLinear(a, b, t), interpolateLinear(b, c, t), t);
}

inline float interpolateQubic(float a, float b, float c, float d, float t)
{
	return interpolateLinear(interpolateQuadratic(a, b, c, t), interpolateQuadratic(b, c, d, t), t);
}

inline float interpolateCosine(float a, float b, float t)
{
	// http://freespace.virgin.net/hugo.elias/models/m_perlin.htm

	float ft = t * glm::pi<float>();
	float f = (1.0f - glm::cos(ft)) * 0.5f;

	return  a * (1.0f - f) + b * f;
}

inline float interpolateAngleLinear(float angleA, float angleB, float t)
{
	float difference = calculateAngleDifference(angleA, angleB);
	angleB = angleA + difference;
	return interpolateLinear(angleA, angleB, t);
}

} }

#endif
