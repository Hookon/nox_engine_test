/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_GRAPHICS_ACTORGRAPHICS_H_
#define NOX_LOGIC_GRAPHICS_ACTORGRAPHICS_H_

#include <nox/logic/actor/Component.h>

#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>

namespace nox
{

namespace app { namespace graphics
{

class SceneGraphNode;
class TransformationNode;

} }

namespace logic
{

namespace actor
{

class Transform;

}

namespace graphics
{

/**
 * Abstract component used by graphics components.
 * Stores the transformation node for the rendered object.
 * Broadcast a SceneNodeCreateEvent when onPostActorCreation() is called
 * containing the transformation node with the node created from
 * the overridden method createSceneNode() as a child node.
 * Updates the transformation node when actor::TransformChange
 * is received.
 *
 *  # JSON Description
 *  - __rotation__:real - How much to rotate the rendered object,
 *  - __scale__:object - How much to scale the rendered object.
 *  	+ __x__:real - Scale in x axis.
 *  	+ __y__:real - Scale in y axis.
 *  - __lightMultiplier__:real - Multiplied with light calculation. 0 means not affected by light, 1 means completely affected by light. Default
 *    is 1.
 *  - __emissiveLight__:real - How much light to emit. Default 0.
 */
class ActorGraphics: public actor::Component
{
public:
	static const IdType NAME;

	~ActorGraphics();

	const IdType& getName() const override;

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	void onDestroy() override;
	void onDeactivate() override;
	void onActivate() override;
	void onComponentEvent(const std::shared_ptr<event::Event>& event) override;

	/**
	 * Set color of an actor.
	 * @param color of the actor.
	 */
	void setColor(const glm::vec4& color);

	/**
	 * Updates the rendering rotation for this actor
	 * @param rotation the new rotation
	 */
	void setRotation(const float rotation);
	void setScale(const glm::vec2& scale);
	void setPosition(const glm::vec2& position);
	const glm::vec2& getScale() const;

	/**
	 * Sets if the object should be rendered or not.
	 * @param state if true the object will be rendered.
	 */
	void enableRendering(const bool state);

	/**
	 * Sets if the rendering should be flipped horizontally.
	 */
	void setFlippedHorizontal(const bool flipped);

	/**
	 * Sets if the rendering should be flipped vertically.
	 */
	void setFlippedVertical(const bool flipped);

	void setActorPositionOffset(const glm::vec2& offset);

protected:
	/**
	 * Create the SceneGraphNode to render.
	 * This must be overrided for each derived class.
	 * This node will be attached to the transform node of
	 * RenderComponent in onPostActorCreation().
	 * @return The node created.
	 */
	virtual std::shared_ptr<app::graphics::SceneGraphNode> createSceneNode() = 0;

	/**
	 * Called when the color of the render component has changed.
	 * @param color The new color.
	 */
	virtual void onColorChange(const glm::vec4& color);

	virtual void onLightMultiplierChange(const float lightMultiplier);
	virtual void onEmissiveLightChange(const float emissiveLight);


private:
	void updateRenderTransform();
	void updateActorTransform();

	void broadcastSceneNodeCreation();
	void broadcastSceneNodeRemoval();

	actor::Transform* transformComponent;

	std::shared_ptr<app::graphics::TransformationNode> actorTransformNode;
	std::shared_ptr<app::graphics::TransformationNode> renderTransformNode;
	glm::vec2 scale;
	glm::vec2 position;
	float rotation;

	bool renderingEnabled;
	glm::vec2 flipScale;
	glm::vec4 color;
	float lightMultiplier;
	float emissiveLight;

	glm::vec2 actorPositionOffset;
};

} } }

#endif
