/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_PHYSICSUTILS_H_
#define NOX_LOGIC_PHYSICS_PHYSICSUTILS_H_

#include <json/json.h>
#include <glm/glm.hpp>
#include <nox/util/boost_utils.h>
#include <nox/util/math/Box.h>

namespace nox { namespace logic { namespace physics
{

enum class PhysicalBodyType
{
	STATIC,
	KINEMATIC,
	DYNAMIC
};

enum class ShapeType
{
	CIRCLE,
	POLYGON,
	RECTANGLE,
	NONE
};

struct BodyShape
{
	BodyShape():
		circleRadius(0.0f),
		type(ShapeType::NONE)
	{}

	math::Box<glm::vec2> box;
	glm::vec2 position;
	util::BoostMultiPolygon polygons;
	float circleRadius;
	ShapeType type;
};

struct BodyDefinition
{
	BodyDefinition():
		linearVelocity(glm::vec2(0.0f)),
		angularVelocity(0.0f),
		angle(0.0f),
		angularDamping(0.0f),
		linearDamping(0.0f),
		density(0.0f),
		friction(0.2f),
		bullet(false),
		sensor(false),
		depth(9),
		bodyType(PhysicalBodyType::DYNAMIC),
		fixedRotation(false),
		gravityMultiplier(1.0f),
		active(true)
	{}

	glm::vec2 position;
	glm::vec2 linearVelocity;
	float angularVelocity;

	float angle;

	float angularDamping;
	float linearDamping;
	float density;
	float friction;
	bool bullet;
	bool sensor;

	unsigned int depth;

	PhysicalBodyType bodyType;
	bool fixedRotation;

	BodyShape shape;
	float gravityMultiplier;

	bool active;
};


bool parseBodyShapeJson(const Json::Value& shapeJson, BodyShape& shape);

BodyShape transformShape(const BodyShape& shape, const glm::vec2& position, const glm::vec2& scale, float rotation);

/**
 * Create a physics shape that is formed as a box.
 * The box will be positioned around [0,0].
 * @param size The size of the box.
 */
BodyShape makeBoxShape(const glm::vec2& size);

} } }

#endif
