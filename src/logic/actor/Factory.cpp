/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/actor/Actor.h>
#include <nox/logic/IContext.h>
#include <nox/logic/actor/Factory.h>
#include <nox/app/resource/data/JsonExtraData.h>
#include <nox/app/resource/IResourceAccess.h>
#include <nox/util/string_utils.h>

#include <memory>
#include <cassert>

namespace nox { namespace logic { namespace actor
{

namespace
{

Actor::ForcedActiveState getForcedActiveStateFromString(const std::string& stateString);

}

Factory::Factory(IContext* logicContext):
	logicContext(logicContext)
{
	assert(this->logicContext != nullptr);

	this->log = this->logicContext->createLogger();
	this->log.setName("ActorFactory");

	this->nextActorId = Identifier::firstValid().getValue();
}

Factory::~Factory()
{
}

void Factory::loadActorDefinitions(app::resource::IResourceAccess* resourceAccess, const std::string& actorDirPath)
{
	const auto actorResources = resourceAccess->getResourcesRecursivelyInDirectory(actorDirPath);
	const auto actorDirTokens = util::splitString<std::vector>(actorDirPath, '/');

	for (const auto& actorResource : actorResources)
	{
		std::vector<ActorDefinition> actorDefinitions = this->getActorDefinitionsFromResource(resourceAccess->getHandle(actorResource), static_cast<unsigned int>(actorDirTokens.size()));

		for (const ActorDefinition& actorDefinition : actorDefinitions)
		{
			const std::string actorPath = actorDefinition.getFullName();
			this->actorDefinitions[actorPath] = actorDefinition;
			this->log.verbose().format("Mapped actor JSON for \"%s\".", actorPath.c_str());
		}
	}
}

std::unique_ptr<Actor> Factory::createActor(const Json::Value& actorJsonObject, std::string definitionName, std::vector<Actor*>& actorChildren)
{
	Json::Value extendValue = actorJsonObject.get("extend", Json::nullValue);

	if (definitionName.empty() == true)
	{
		definitionName = actorJsonObject.get("extend", "").asString();
	}

	std::string actorName = this->extractNameFromDefinitionName(definitionName);

	bool trivialActor = actorJsonObject.get("trivial", false).asBool();

	Actor::ForcedActiveState forcedActiveState = Actor::ForcedActiveState::NONE;

	const auto actorId = Identifier{this->nextActorId};
	this->nextActorId++;

	std::vector<std::string> definitionsExtendedFrom;

	std::stack<Json::Value> actorExtendStack;
	actorExtendStack.push(actorJsonObject);

	while (extendValue.isNull() == false)
	{
		const std::string extendedDefinition = extendValue.asString();
		auto actorJsonIt = this->actorDefinitions.find(extendedDefinition);

		if (actorJsonIt != this->actorDefinitions.end())
		{
			const Json::Value& extendJsonRoot = actorJsonIt->second.json;
			actorExtendStack.push(extendJsonRoot);
			definitionsExtendedFrom.push_back(extendedDefinition);

			extendValue = extendJsonRoot.get("extend", Json::nullValue);
		}
		else
		{
			this->log.error().format("Could not find actor \"%s\".", extendedDefinition.c_str());
			extendValue = Json::nullValue;
		}
	}

	const Json::Value& topActorJson = actorExtendStack.top();

	actorName = topActorJson.get("name", actorName).asString();
	trivialActor = topActorJson.get("trivial", trivialActor).asBool();

	if (topActorJson["forcedActiveState"].isString() == true)
	{
		forcedActiveState = getForcedActiveStateFromString(topActorJson["forcedActiveState"].asString());
	}

	Json::Value actorComponents = topActorJson["components"];
	Json::Value childActors;

	const Json::Value& extendedChildActors = topActorJson["childActors"];
	const Json::Value::Members extendedChildNames = extendedChildActors.getMemberNames();
	for (const std::string& childName : extendedChildNames)
	{
		childActors[childName] = extendedChildActors[childName];
	}

	actorExtendStack.pop();

	while (actorExtendStack.empty() == false)
	{
		const Json::Value& actorJson = actorExtendStack.top();

		actorName = actorJson.get("name", actorName).asString();
		trivialActor = actorJson.get("trivial", trivialActor).asBool();

		if (actorJson["forcedActiveState"].isBool() == true)
		{
			forcedActiveState = getForcedActiveStateFromString(topActorJson["forcedActiveState"].asString());
		}

		const Json::Value& componentMap = actorJson["components"];
		this->extendActorComponents(actorComponents, componentMap);

		const Json::Value& extendedChildActors = actorJson["childActors"];
		const Json::Value::Members extendedChildNames = extendedChildActors.getMemberNames();
		for (const std::string& childName : extendedChildNames)
		{
			childActors[childName] = extendedChildActors[childName];
		}

		actorExtendStack.pop();
	}

	auto actor = std::unique_ptr<Actor>(new Actor(this->logicContext, actorId, actorName, definitionName, definitionsExtendedFrom, this->logicContext->createLogger()));
	actor->forceActiveState(forcedActiveState);
	actor->makeTrivial(trivialActor);

	if (actorComponents.empty())
	{
		this->log.warning().raw("Creating actor with no components.");
	}

	const auto componentNameList = actorComponents.getMemberNames();

	for (const auto& componentName : componentNameList)
	{
		const Json::Value& componentObject = actorComponents[componentName];

		std::unique_ptr<Component> component = this->createComponent(componentName, componentObject);
		if (component != nullptr)
		{
			if (actor->addComponent(component) == false)
			{
				this->log.warning().format("Component %s won't be attached to the actor since the actor already has one with the same name.", component->getName().c_str());
			}
		}
	}

	const Json::Value::Members childActorNames = childActors.getMemberNames();

	for (const std::string& childName : childActorNames)
	{
		const Json::Value childJson = childActors[childName];
		std::string childDefinitionName = definitionName + '.' + childName;

		if (childJson["extend"].isNull() == false)
		{
			childDefinitionName = childJson["extend"].asString();
		}

		std::unique_ptr<Actor> childActor = this->createActor(childJson, childDefinitionName, actorChildren);
		if (childActor != nullptr)
		{
			actorChildren.push_back(childActor.get());
			actor->attachChildActor(std::move(childActor), childName);
		}
		else
		{
			this->log.error().format("Child actor \"%s\" could not be created.", childName.c_str());
		}
	}

	return actor;
}

std::unique_ptr<Actor> Factory::createActor(const std::string& actorDefinitionName, std::vector<Actor*>& actorChildren)
{
	auto actorDefinitionIt = this->actorDefinitions.find(actorDefinitionName);

	if (actorDefinitionIt != this->actorDefinitions.end())
	{
		const Json::Value& actorJsonRoot = actorDefinitionIt->second.json;

		return this->createActor(actorJsonRoot, actorDefinitionName, actorChildren);
	}

	this->log.error().format("Could not create actor: Actor \"%s\" not found.", actorDefinitionName.c_str());
	return nullptr;
}

std::unique_ptr<Component> Factory::createComponent(const std::string& name, const Json::Value& componentJsonObject)
{
	std::unique_ptr<Component> component = this->componentFactory.createObject(name);
	if (component == nullptr)
	{
		this->log.error().raw("Failed creating component from json: Component name \"" + name + "\" invalid.");
		return std::unique_ptr<Component>();
	}

	component->setContext(this->logicContext);

	if (component->initialize(componentJsonObject) == false)
	{
		this->log.error().format("Failed initializing component %s", name.c_str());
		return std::unique_ptr<Component>();
	}

	return component;
}

void Factory::extendJsonValue(Json::Value& extendedValue, const Json::Value& extendWithValue)
{
	if (extendWithValue.isObject() == true && extendedValue.isObject() == true)
	{
		const Json::Value::Members& extendWithMembers = extendWithValue.getMemberNames();

		for (const std::string& extendWithMemberName : extendWithMembers)
		{
			const Json::Value& extendWithMember = extendWithValue[extendWithMemberName];
			Json::Value& extendedMember = extendedValue[extendWithMemberName];

			this->extendJsonValue(extendedMember, extendWithMember);
		}
	}
	else if (extendWithValue.isArray() == true && extendedValue.isArray() == true)
	{
		for (Json::ArrayIndex i = 0; i < extendWithValue.size(); i++)
		{
			this->extendJsonValue(extendedValue[i], extendWithValue[i]);
		}
	}
	else
	{
		extendedValue = extendWithValue;
	}
}

void Factory::resetCounter()
{
	this->nextActorId = 0;
}

void Factory::flushActorDefinitionCache()
{
	this->actorDefinitions.clear();
}

void Factory::extendActorComponents(Json::Value& componentsToExtend, const Json::Value& componentsExtendingFrom)
{
	const auto componentNameList = componentsExtendingFrom.getMemberNames();

	for (const auto& componentName : componentNameList)
	{
		auto& existingComponent = componentsToExtend[componentName];

		if (existingComponent.isNull() == false)
		{
			this->extendJsonValue(existingComponent, componentsExtendingFrom[componentName]);
		}
		else
		{
			componentsToExtend.append(componentsExtendingFrom[componentName]);
		}
	}
}

std::vector<Factory::ActorDefinition> Factory::getActorDefinitionsFromResource(const std::shared_ptr<app::resource::Handle>& actorResourceHandle, const unsigned int rootDirPathLength)
{
	if (actorResourceHandle == nullptr)
	{
		this->log.error().format("Could not get resource handle for actor definition.");
		return {};
	}

	const auto actorResourceExtra = actorResourceHandle->getExtraData<app::resource::JsonExtraData>();

	if (actorResourceExtra == nullptr)
	{
		this->log.error().format("Could not get JSON data for actor definition: %s", actorResourceHandle->getResourceDescriptor().getPath().c_str());
		return {};
	}

	const Json::Value& resourceJson = actorResourceExtra->getRootValue();

	const std::vector<std::string> actorNames = resourceJson.getMemberNames();

	std::deque<std::string> actorResourceTokens = util::splitString<std::deque>(actorResourceHandle->getResourceDescriptor().getPath(), '/');

	// Remove the actor directory from the actor tokens.
	actorResourceTokens.erase(actorResourceTokens.begin(), actorResourceTokens.begin() + rootDirPathLength);

	// Remove the filename from the actor tokens.
	actorResourceTokens.pop_back();

	std::string actorPackage;

	for (const auto& token : actorResourceTokens)
	{
		if (token.find('.') != std::string::npos)
		{
			this->log.error().format("Actor package \"%s\" contains punctuation, which is not allowed.", actorResourceHandle->getResourceDescriptor().getPath().c_str());
		}
		else
		{
			actorPackage += token + '.';
		}
	}

	if (actorResourceTokens.empty() == false)
	{
		actorPackage += actorResourceTokens.back();
	}

	std::vector<ActorDefinition> actorPackageDefinitions;

	for (const std::string& actorName : actorNames)
	{
		ActorDefinition actorDefinition;
		actorDefinition.package = actorPackage;
		actorDefinition.name = actorName;
		actorDefinition.json = resourceJson[actorName];

		actorPackageDefinitions.push_back(actorDefinition);

		const Json::Value& actorJson = resourceJson[actorName];
		const Json::Value& childrenJson = actorJson["childActors"];

		if (childrenJson.isNull() == false)
		{
			const std::vector<std::string> childNames = childrenJson.getMemberNames();
			for (const std::string& childName : childNames)
			{
				ActorDefinition actorDefinition;
				actorDefinition.package = actorPackage;
				actorDefinition.name = actorName + '.' + childName;
				actorDefinition.json = childrenJson[childName];

				actorPackageDefinitions.push_back(actorDefinition);
			}
		}
	}

	return actorPackageDefinitions;
}

std::string Factory::ActorDefinition::getFullName() const
{
	if (this->package.empty() == true)
	{
		return this->name;
	}
	else
	{
		return this->package + '.' + this->name;
	}
}

std::string Factory::extractNameFromDefinitionName(const std::string& definitionName) const
{
	const auto definitionIt = this->actorDefinitions.find(definitionName);

	if (definitionIt != this->actorDefinitions.end())
	{
		const ActorDefinition& definition = definitionIt->second;
		return definition.name;
	}

	return std::string();
}

namespace
{

Actor::ForcedActiveState getForcedActiveStateFromString(const std::string& stateString)
{
	if (stateString == "active")
	{
		return Actor::ForcedActiveState::ACTIVE;
	}
	else if (stateString == "inactive")
	{
		return Actor::ForcedActiveState::INACTIVE;
	}
	else
	{
		return Actor::ForcedActiveState::NONE;
	}
}

}

} } }
