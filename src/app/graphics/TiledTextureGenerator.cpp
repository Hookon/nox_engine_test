/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/TiledTextureGenerator.h>

#include <nox/app/graphics/opengl/TextureManager.h>
#include <nox/app/graphics/opengl/TiledTextureRenderer.h>

namespace nox { namespace app
{
namespace graphics
{

TiledTextureGenerator::TiledTextureGenerator(const TextureManager& textureManager, TiledTextureRenderer* renderer) :
	textureManager(textureManager),
	tileRenderer(renderer),
	quadSize(10.0f, 10.0f),
	tileColor(1.0f),
	tileLightLuminance(1.0f, 0.0f),
	currentMinTiles(0, 0),
	currentMaxTiles(0, 0),
	SMALL_OVERLAP(0.01f)
{
}

TiledTextureGenerator::~TiledTextureGenerator()
{
}

void TiledTextureGenerator::onUpdate(glm::vec2 currentPosition, glm::vec2 currentCoverage)
{
	glm::vec2 lowerLeftBounds = currentPosition - (currentCoverage / 2.0f);
	glm::vec2 upperRightBounds = currentPosition + (currentCoverage / 2.0f);

	if (this->needToUpdateTexture(lowerLeftBounds, upperRightBounds) && this->textures.empty() == false)
	{
		this->generateTexture();
		this->tileRenderer->updateTextureQuads(&this->textureQuads);
	}
}

bool TiledTextureGenerator::needToUpdateTexture(glm::vec2 lowerLeftBounds, glm::vec2 upperRightBounds)
{
	bool needToUpdate = false;

	glm::vec2 tilesThatWillFitFractionMin = (lowerLeftBounds/this->quadSize);
		
	if (tilesThatWillFitFractionMin.x < 0)
	{
		tilesThatWillFitFractionMin.x--;
	}
	
	if (tilesThatWillFitFractionMin.y < 0)
	{
		tilesThatWillFitFractionMin.y--;
	}
	
	glm::ivec2 tilesThatWillCoverAreaMin = glm::ivec2((int)tilesThatWillFitFractionMin.x - 1,
							  (int)tilesThatWillFitFractionMin.y - 1);
	if (this->currentMinTiles != tilesThatWillCoverAreaMin)
	{
		this->currentMinTiles = tilesThatWillCoverAreaMin;
		needToUpdate = true;
	}
	
	glm::vec2 tilesThatWillFitFractionMax = (upperRightBounds/this->quadSize);
	glm::ivec2 tilesThatWillCoverAreaMax = glm::ivec2((int)tilesThatWillFitFractionMax.x + 3,
							  (int)tilesThatWillFitFractionMax.y + 3);
	if (this->currentMaxTiles != tilesThatWillCoverAreaMax)
	{
		this->currentMaxTiles = tilesThatWillCoverAreaMax;
		needToUpdate = true;
	}
	
	return needToUpdate;
}

void TiledTextureGenerator::generateTexture()
{
	using SizeType = std::vector<TextureQuad::RenderQuad>::size_type;

	const SizeType numTiles = static_cast<SizeType>((currentMaxTiles.x - currentMinTiles.x) * (currentMaxTiles.y - currentMinTiles.y));
	this->textureQuads.resize(numTiles);
	
	SizeType index = 0;

	for (int y = currentMinTiles.y; y < currentMaxTiles.y ; y++)
	{
		for (int x = currentMinTiles.x; x < currentMaxTiles.x; x++)
		{
			float actualX = static_cast<float>(x) - this->SMALL_OVERLAP;
			float actualY = static_cast<float>(y) - this->SMALL_OVERLAP;
			
			const glm::vec2 bottomLeft = glm::vec2(this->quadSize.x * actualX, this->quadSize.y * actualY);
			
			actualX = static_cast<float>(x + 1) + this->SMALL_OVERLAP;
			actualY = static_cast<float>(y + 1) + this->SMALL_OVERLAP;
			
			const glm::vec2 topRight = glm::vec2(this->quadSize.x * actualX, this->quadSize.y * actualY);
			
			TextureQuad::RenderQuad& quad = this->textureQuads[index];

			quad = textures[0].getRenderQuad();

			quad.bottomLeft.setPosition(bottomLeft);
			quad.bottomRight.setPosition({topRight.x, bottomLeft.y});
			quad.topRight.setPosition(topRight);
			quad.topLeft.setPosition({bottomLeft.x, topRight.y});
			
			quad.setColor(this->tileColor);

			quad.setLightMultiplier(this->tileLightLuminance.x);
			quad.setEmissiveLight(this->tileLightLuminance.y);

			++index;
		}
	}
}

bool TiledTextureGenerator::addTextureTile(const std::string& textureName)
{
	if (this->textureManager.hasTexture(textureName))
	{
		this->textures.push_back(this->textureManager.getTexture(textureName));
		return true;
	}
	else
	{
		return false;
	}
}

void TiledTextureGenerator::setTileSize(const glm::vec2& size)
{
	this->quadSize = size;
}

void TiledTextureGenerator::setTileRenderer(TiledTextureRenderer* renderer)
{
	this->tileRenderer = renderer;
}

void TiledTextureGenerator::setTileColor(const glm::vec4& color)
{
	this->tileColor = color;
}

}
} }
